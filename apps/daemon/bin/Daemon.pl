#!/usr/bin/perl -w
use strict;
use lib '../../../module/lib';
use POE;
use POSIX;
use MyGSM;
use MyGSM::Model;
use YAML::Syck;
use Data::Dumper;
use Encode;

$|++;

POE::Session->create(
	inline_states =>
	{
		_start => \&gsm_init,
		_stop => \&gsm_stop,
		gsm_read => \&gsm_read,
		gsm_send_statistics => \&gsm_send_statistics,
		db_init => \&db_init,
		db_insert => \&db_insert,
	}
);


print "Daemon started!\n";
POE::Kernel->run();
print "Daemon stoped!\n";
exit;


sub gsm_init {
	print "daemon starting,SID=",$_[SESSION]->ID,"\n";

	my $config = $_[HEAP]->{config} = LoadFile( q( ../../config/MyGsmApp.conf ) );
	my $appatch = $config->{MyGsmAppDaemon}->{AppPath};

	my $gsm = MyGSM->new( 
								port => $config->{MyGsmAppDaemon}->{Device}->{Port},
								log => 'file,'.$appatch.$config->{MyGsmAppDaemon}->{Log}->{File},
								loglevel => $config->{MyGsmAppDaemon}->{Log}->{Level},
							);

	$gsm->connect(baudrate => $config->{MyGsmAppDaemon}->{Device}->{Baudrate});
	my $connected =$gsm->devStatus->{factorer};
	sleep 5; 
	if ( $connected ) {
		print "Connected!\n";
		$_[HEAP]->{gsm} = $gsm; 
		sleep 10;
		$_[KERNEL]->yield("db_init");
	}else{
		print "GSM Modem connection failed!\n";
	}
}

sub gsm_stop {
	print $_[SESSION]->ID," stoped!\n";
}


sub gsm_read {
	print "in gsm_read\n";
	my $gsm = $_[HEAP]->{gsm};
	
	for my $record ($gsm->read()){
		print Dumper $record;
		if ( defined $record->{index} ) {
			$gsm->del($record->{index});
			if ( $record->{content} =~ m/^0000$/o ) {
				$_[KERNEL]->yield("gsm_send_statistics",$record->{cellphone});
			} else {
				$_[KERNEL]->yield("db_insert",$record);
			}
		}
	}
	sleep 5;
	$_[KERNEL]->yield("gsm_read");

}

sub gsm_send_statistics {
	my $db = $_[HEAP]->{db};
	my $gsm = $_[HEAP]->{gsm};
	my $content;
	my ( $year,$month,$day ) = split(/,/,strftime("%Y,%m,%d",localtime()));
	print "in gsm_send_statistics!\n";

	my  ($in_month,$a )= $db->query(
		'select sum(fee),min(day) from account where fee>0 and month="'
		.$month
		.'" and cellphone ="'
		.$_[ARG0]
		.'"')->fetchrow_array;
		
	my ($out_month,$b ) = $db->query(
		'select sum(fee),min(day)  from account where fee<0 and month="'
		.$month
		.'" and cellphone ="'
		.$_[ARG0]
		.'"')->fetchrow_array;
		
	$out_month = abs($out_month);
	my $sub=$in_month-$out_month;
	my $min_day = $a>$b ? $b : $a;
	my $days = $day-$min_day+1;
	my $in_per_day = sprintf( "%.2f",$in_month/$days );
	my $out_per_day = sprintf( "%.2f",$out_month/$days );
	
	$content=<<"EOF";
本月$days天收入$in_month元,支出$out_month元,收支差为$sub元,平均每天收入$in_per_day元,支出$out_per_day元
EOF

	$gsm->send(
		recipient => $_[ARG0],
		content => $content,
	);
	$_[KERNEL]->yield('gsm_read');	
}

sub db_init {
	my $config = $_[HEAP]->{config};
	my $appatch = $config->{MyGsmAppDaemon}->{AppPath};

	my $dsn     = {
		driver   => $config->{MyGsmAppDaemon}->{Database}->{Driver},
		host     => $config->{MyGsmAppDaemon}->{Database}->{Host},
		dbname   => $appatch.$config->{MyGsmAppDaemon}->{Database}->{File},
		user     => $config->{MyGsmAppDaemon}->{Database}->{User},
		password => $config->{MyGsmAppDaemon}->{Database}->{Password},
	};
	$_[HEAP]->{db} = MyGSM::Model->init_db($dsn);
	
	print "db inited!\n" if $_[HEAP]->{db}->connected;
	$_[KERNEL]->yield("gsm_read");
}


sub db_insert {
	print "in db_insert\n";
	my $db = $_[HEAP]->{db};
	my $data = $_[ARG0];
	print "=========\n",Dumper $data,"====\n";
	my ( $in_or_out,$fee,$describe );
	while($data->{content} =~ /^\s*([\+\-]\d+\.?\d*?)(.+?)$/gsmo){

		if ( $1 && $2  ) {
			$db->insert(
				'account',
				{
					cellphone => $data->{cellphone},
					fee => $1,
					year => $data->{year},
					month => $data->{month},
					day => $data->{day},
					time =>$data->{time},
					describe => $2,
				},
			);
		}
	}
	
	$_[KERNEL]->yield("gsm_read");
}