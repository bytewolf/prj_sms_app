package MyApp;

use strict;
use FindBin;
use Cwd qw/abs_path/;
use lib abs_path("$FindBin::Bin/../lib");
use Data::Dumper;
use base qw/CGI::Application MyApp::Tools/;
use CGI::Carp qw/fatalsToBrowser/;
use CGI::Application::Plugin::Config::YAML;
use MyApp::Conf;
use MyApp::M::DBIx;
use MyApp::V::Template;

sub cgiapp_init {
    my $self = shift;

    #############
    # DSN initialize
    #############
   $self->{'dsn'} = $self->get_dsn( $self->get_conf );
}


sub cgiapp_postrun {
    my $self = shift;
    $self->header_add( -charset => 'UTF-8' );
}

sub setup {
    my $self = shift;
    $self->start_mode('index');
    $self->mode_param('rm');
    $self->run_modes( $self->get_runmodes );
    $self->tmpl_path( $self->{'conf'}->{'web'}->{'templates'} );
}


sub teardown {
    my $self = shift;
#    $self->{'dsn'}->finish;
 #   $self->{'dsn'}->disconnect;
}


sub index {
	my $self = shift;
	my $t = $self->load_tmpl('index.tmpl');
	$t->param( HOME =>$self->{'conf'}->{'web'}->{'home'} );
	return $t->output;
}


1;