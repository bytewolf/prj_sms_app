package MyApp::C::Search;

use strict;
use FindBin;
use Cwd qw/abs_path/;
use lib abs_path("$FindBin::Bin/../lib");
use base 'MyApp';

sub search {
	my $self = shift;
	my $db = $self->{dsn};
	my $cellphone = $self->param('c');
	my $y = $self->query->param('y');
	if ( $cellphone && $y ) {
		$self->args_error("参数错误!") 
			unless ( $cellphone =~ /^\+86\d{11}$/ && $y=~ /^\+86\d{11}$/ );
	}

	my $years = $db->query(
		'select distinct(year) from account where cellphone="'
		.$cellphone
		.'"')->fetchall_arrayref;
		
	my $t = $self->load_tmpl('search_search.tmpl');
	$t->param( 
		HOME => $self->{conf}->{web}->{home},
		CELLPHONE => $cellphone,
		YEAR_LIST =>[
			map {
				{
					YEAR => $_->[0],
				}
			} @{$years}
		],
	);

	if ( $y ) {
		my $result =	$db->query(
			'select * from account where cellphone="'
			.$cellphone.'"'
			.' and year="'
			.$y.'"')->fetchall_arrayref;
		my  ( $in ) = $db->query(
			'select sum(fee) from account where fee>0 and cellphone="'
			.$cellphone.'"'
			.' and year="'
			.$y.'"')->fetchrow_array;
			
		my ( $out  ) = $db->query(
			'select sum(fee) from account where fee<0 and cellphone="'
			.$cellphone.'"'
			.' and year="'
			.$y.'"')->fetchrow_array;
			
		$out = abs($out);

		$t->param(
			TOTAL_IN => $in||0,
			TOTAL_OUT => $out||0,
			TOTAL => $in-$out,
			RECORDS => [
				map {
					{
						HOME => $self->{conf}->{web}->{home},
						ID => $_->[0],
						CELLPHONE => $_->[1],
						FEE => $_->[2],
						DATETIME => $_->[3].'/'.$_->[4].'/'.$_->[5].' '.$_->[6],
						DESCRIBE => $_->[7],
					}
				} @{$result}
			],
		);
		
		return $t->output;
	
	} else {
		my $result =	$db->query(
			'select * from account where cellphone="'
			.$cellphone.'"'
			.' and year=('
			.'select max(year) from account where cellphone="'
			.$cellphone.'")')->fetchall_arrayref;
		my  ( $in ) = $db->query(
			'select sum(fee) from account where fee>0 and cellphone="'
			.$cellphone.'"'
			.' and year="'
			.$result->[0]->[3].'"')->fetchrow_array;
			
		my ( $out  ) = $db->query(
			'select sum(fee) from account where fee<0 and cellphone="'
			.$cellphone.'"'
			.' and year="'
			.$result->[0]->[3].'"')->fetchrow_array;
			
		$out = abs($out);

		$t->param(
			TOTAL_IN => $in,
			TOTAL_OUT => $out,
			TOTAL => $in-$out,
			RECORDS => [
				map {
					{
						HOME => $self->{conf}->{web}->{home},
						ID => $_->[0],
						CELLPHONE => $_->[1],
						FEE => $_->[2],
						DATETIME => $_->[3].'/'.$_->[4].'/'.$_->[5].' '.$_->[6],
						DESCRIBE => $_->[7],
					}
				} @{$result}
			],
		);
		
		return $t->output;
	}
	
	$self->args_error("参数错误!");
}


1;