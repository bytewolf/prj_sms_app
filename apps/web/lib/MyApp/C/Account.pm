package MyApp::C::Account;

use strict;
use FindBin;
use Cwd qw/abs_path/;
use lib abs_path("$FindBin::Bin/../lib");
use base 'MyApp';

sub stat {
	my $self = shift;
	my $db = $self->{dsn};
	my $cellphone = '+86'.( $self->param("var") || $self->query->param("var") );
	$cellphone =~ s/(?:\+86){2}/\+86/;
	$self->args_error("手机号码格式有误!") unless $cellphone =~ /^\d{11}$/;
	my $t = $self->load_tmpl('account_stat.tmpl');
	$db->select(
		{
			fields => '*',
			table  => 'account',
			order  => 'id',
			where => { 'cellphone' => $cellphone },
		}
	);
	my $result = $db->fetchall_arrayref;
	
	my  ( $in ) = $db->query(
		'select sum(fee) from account where fee>0 and cellphone="'
		.$cellphone
		.'"')->fetchrow_array;
		
	my ( $out  ) = $db->query(
		'select sum(fee) from account where fee<0 and cellphone="'
		.$cellphone
		.'"')->fetchrow_array;
		
	$out = abs($out);
	
	if ( @{$result} ) {
		$t->param(
			HOME => $self->{conf}->{web}->{home},
			CELLPHONE =>$cellphone,
			TOTAL_IN => $in||0,
			TOTAL_OUT => $out||0,
			TOTAL => $in-$out,
			RECORDS => [
				map {
					{
						HOME => $self->{conf}->{web}->{home},
						ID => $_->[0],
						CELLPHONE => $_->[1],
						FEE => $_->[2],
						DATETIME => $_->[3].'/'.$_->[4].'/'.$_->[5].' '.$_->[6],
						DESCRIBE => $_->[7],
					}
				} @{$result}
			]
		);
		return $t->output;
	} else {
		$self->args_error('数据库中没有匹配该手机的记录!');
	}

}


1;