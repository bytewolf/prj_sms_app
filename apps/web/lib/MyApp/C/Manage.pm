package MyApp::C::Manage;

use strict;
use FindBin;
use Cwd qw/abs_path/;
use lib abs_path("$FindBin::Bin/../lib");
use base 'MyApp'; 

sub edit {
	my $self = shift;
	my $db = $self ->{dsn};
	my $cellphone = $self->param('var');
	my $id = $self->param('var2');
	warn "=====$id,$cellphone===";
	my $q = $self->query();
	my $t = $self->load_tmpl("manage_edit.tmpl");
	
	$self->args_error("参数不正确!") unless ($id =~ /^\d+$/ && $cellphone =~ /^\+86\d{11}$/);
	
	$t->param( HOME => $self->{conf}->{web}->{home} );
	
	if ( $q->param('do_edit') ) {
		$db->update(
			'account',
			{
				cellphone => $q->param('cellphone'),
				fee => $q->param('fee'),
				year => $q->param('year'),
				month => $q->param('month'),
				day => $q->param('day'),
				time => $q->param('time'),
				describe => $q->param('describe'),
			},
			{ id => $id },
		);
		
		if ( $db->rows ) {
			return "<script>alert('修改记录 $id 成功!');window.location=\"$ENV{'HTTP_REFERER'}\"</script>";
		} else {
			return "<script>alert('修改记录 $id 失败!');window.location=\"$ENV{'HTTP_REFERER'}\"</script>";
		}
		
	} else {
		my $result = $db->select_one_to_hashref('*','account',{id=>$id});

		$t->param(
				ID => $result->{id},
				CELLPHONE => $result->{cellphone},
				FEE => $result->{fee},
				YEAR => $result->{year},
				MONTH => $result->{month},
				DAY => $result->{day},
				TIME => $result->{time},
				DESCRIBE => $result->{describe},
		);
		return $t->output;
	}

}

sub del {
	my $self = shift;
	my $db = $self->{dsn};
	my $cellphone = $self->param('var');
	my $id = $self->param('var2');
	
	$self->args_error("参数不正确!") unless ($id =~ /^\d+$/ && $cellphone =~ /^\+86\d{11}$/);

	$db->delete('account',{ id=>$id });
	if ( $db->rows ) {
		return "<script>alert('成功删除记录 $id !');window.location=\"$ENV{'HTTP_REFERER'}/$cellphone\"</script>";
	} else {
		return "<script>alert('删除记录 $id失败!');window.location=\"$ENV{'HTTP_REFERER'}/$cellphone\"</script>";

	}
}

1;