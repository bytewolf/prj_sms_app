package MyApp::Conf;

use strict;
use FindBin;
use Cwd qw/abs_path/;
use base qw/Exporter/;
use vars qw/@EXPORT/;
@EXPORT = qw/get_conf get_runmodes/;

my $conf_file = "MyGsmApp.conf";

sub get_runmodes {
    my $run_modes = {
### Configure run modes here ###
        'MyApp' => [ qw/index/ ],

	'MyApp::C::Account' => [
		qw/stat/
	],
	
	'MyApp::C::Search' => [
		qw/search/
	],
	
	'MyApp::C::Manage' => [
		qw/edit del/
	],

### END ###
    };
    my %modes = ();
    return [grep { !$modes{$_}++ } map { @{$_} } values %{$run_modes}];
}

sub get_conf {
    #############
    # MyGsmAppWeb configure file
    #############
    my $self = shift;
    my $CONF = abs_path("$FindBin::Bin/../../config") . "/$conf_file";
    $self->config_file($CONF);

    my $app_path = $self->config_param('MyGsmAppWeb')->{'AppPath'};
    $self->{'conf'}->{'lib'} = $app_path . 'lib/';
    $self->{'conf'}->{'web'}->{'home'} =
    $self->config_param('MyGsmAppWeb')->{'Web'}->{'HomeURI'};
    $self->{'conf'}->{'web'}->{'static'} =
    $app_path . $self->config_param('MyGsmAppWeb')->{'Web'}->{'Static'};
    $self->{'conf'}->{'web'}->{'templates'} =
    $app_path . $self->config_param('MyGsmAppWeb')->{'Web'}->{'TemplateRoot'};

    my $data_path = $self->config_param('MyGsmAppWeb')->{'Database'};
    $self->{'conf'}->{'data'}->{'driver'}   = $data_path->{'Driver'};
    $self->{'conf'}->{'data'}->{'file'}     = $app_path . $data_path->{'File'};
    $self->{'conf'}->{'data'}->{'host'}     = $data_path->{'Host'};
    $self->{'conf'}->{'data'}->{'user'}     = $data_path->{'User'};
    $self->{'conf'}->{'data'}->{'password'} = $data_path->{'Password'};
    
    return $self->{'conf'};
}

1;
