package MyApp::Dispatch;

use base 'CGI::Application::Dispatch';
use CGI::Carp qw/fatalsToBrowser/;

$ENV{PATH_INFO} = lc $ENV{PATH_INFO} if defined $ENV{PATH_INFO};

sub dispatch_args {
        return {
            prefix  => '',
            table   => [
                'search/:c/:y?/:m?' => {prefix=>'MyApp::C',app=>'Search',rm=>'search'},
                ':app/:rm/:var?/:var2?' => {prefix=>'MyApp::C'},
	        ':rm' => {app=>'MyApp'},
                '' => {app=>'MyApp'},
            ],
        };
    }
    
 1;
