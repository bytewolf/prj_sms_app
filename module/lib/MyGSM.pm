package MyGSM;
use strict;
use Device::Gsm 1.48;
use Data::Dumper;
use Encode;
our @ISA = qw(Device::Gsm);



sub devStatus {
	my $self = shift;
	
	return {
		csq => $self->signal_quality(),
		imei => $self->imei(),
		factorer => $self->manufacturer(),
		bands => $self->model(),
		datetime => $self->datetime(),
		csca => $self->service_center(),
		imsi => $self->_get_result_of_at('CIMI'),
		sim => $self->_get_result_of_at('CCID'),
		version => $self->_get_result_of_at('CGMR'),
	};
}

sub read {
	my $self = shift;
	$self->mode('pdu');
	$self->echo(0);
	$self->atsend( q{AT+CMGL=0} . Device::Modem::CR);
	my $messages = $self->answer($Device::Modem::STD_RESPONSE,5000);
	#$messages =~ s/AT\+CMGL=0[\n\r]*//o;
	while ( my $more= $self->answer($Device::Modem::STD_RESPONSE,600)) {
        #-- $self->answer will chomp trailing newline, add it back
	$messages .= "\n";
	$messages .= $more;

	}
	
	print "//////// ",Dumper $messages,"///////////\n";

	# Ok, messages read, now convert from PDU and store in object
	$self->log->write('debug', 'Messages='.$messages );
	my @data = split /[\r\n]+/m, $messages;
	# Check for errors on SMS reading
	my $code;
	if( ($code = pop @data) =~ /ERROR/ ) {
		$self->log->write('error', 'cannot read SMS messages on SIM: ['.$code.']');
		return ();
	}

	my @message = ();
	my $current;

    # Current sms storage memory (ME or SM)
    my $storage = 'SM';

	#
	# Parse received data (result of +CMGL command)
	#
	while( @data ) {
		$self->log->write('debug', 'data[] = ', $data[0] );
		my $header=shift @data;
		my $pdu   =shift @data;
		# Instance new message object
		my $msg = new Device::Gsm::Sms(
			header => $header,
			pdu    => $pdu,
            # XXX mode   => $self->mode(),
            storage => $storage,
            parent => $self                  # Ref to parent Device::Gsm class
		);

		# Check if message has been instanced correctly
		if( ref $msg ) {
			my $SCTS = $msg->{tokens}->{SCTS};
			push @message, {
 				index => $msg->{index},
				cellphone => $msg->{tokens}->{OA}->{_address},
				content => encode_utf8( $msg->{tokens}->{UD}->{_text} ),
				year => $SCTS->{_year}+2000,
				month => $SCTS->{_month},
				day => $SCTS->{_day},
				time => $SCTS->{_time},
			};
		} else {
			$self->log->write('info', "could not instance message $header $pdu!");
		}
	}
	$self->log->write('info', 'found '.(scalar @message).' messages on SIM. Reading.');
	return @message;
}


sub send {
	my($self, %opt) = @_;

	$self->atsend(  q[ATE1] . Device::Modem::CR );
	$self->answer($Device::Modem::STD_RESPONSE);

	my $send_Ok = 0;
	my $Reply;
	my $pdu = $self->_encode_pdu(%opt);
	return "ERROR!\n" unless $pdu;
	$self->log->write('info', 'due to send PDU ['.$pdu.']');

	# Sending main SMS command ( with length )
	my $len = ( (length $pdu) >> 1 ) - 1;

	#$me->log->write('info', 'AT+CMGS='.$len.' string sent');
	# Select PDU format for messages
	$self->atsend(  q[AT+CMGF=0] . Device::Modem::CR );
	$self->answer($Device::Modem::STD_RESPONSE);
	$self->log->write('info', 'Selected PDU format for msg sending');

	# Send SMS length
	$self->atsend( qq[AT+CMGS=$len] . Device::Modem::CR );
	$self->answer($Device::Modem::STD_RESPONSE);

	# Sending SMS content encoded as PDU
	$self->log->write('info', 'PDU sent ['.$pdu.' + CTRLZ]' );
	$self->atsend( $pdu . Device::Modem::CTRL_Z );

	# Get reply and check for errors
	$Reply = $self->answer($Device::Modem::STD_RESPONSE, 30000);
	$self->log->write( 'debug', "SMS reply: $Reply\r\n" );

	if( $Reply =~ /ERROR/i ) {
		$Reply =~ /(\+CMGS:.*)/;
		$self->log->write( 'warning', "ERROR in sending SMS: $1" );
	} else {
		$self->log->write( 'info', "Sent SMS (pdu mode) to $opt{'recipient'}!" );
		$send_Ok = 1;
	}

	return $send_Ok;
}


sub del {
	my $self      = shift;
    	my $msg_index = shift;
    	my $del_opt = shift||'index';
    	my $storage   = shift||'SM';
    	my $ok;

    	if( ! defined $msg_index || $msg_index eq '' ) {
        	$self->log->write('warn', 'undefined message number. cannot delete sms message');
        	return 0;
    	}

	# Set default SMS storage if supported
	$self->storage($storage);
	my $ans;
	if ( $del_opt eq 'all_read' ) {
		$self->atsend( qq{AT+CMGD=$msg_index,1} . Device::Modem::CR );
		$ans = $self->parse_answer($Device::Modem::STD_RESPONSE,30000);
	} elsif ( $del_opt eq 'all' ) {
		$self->atsend( qq{AT+CMGD=$msg_index,4} . Device::Modem::CR );
		$ans = $self->parse_answer($Device::Modem::STD_RESPONSE,80000);
	} else {
		$self->atsend( qq{AT+CMGD=$msg_index} . Device::Modem::CR );
		$ans = $self->parse_answer($Device::Modem::STD_RESPONSE,5000);
	}

	if( index($ans, 'OK') > -1 || $ans =~ /\+CMGD/ ) {
		$ok = 1;
	}
	
	$self->log->write('info', "deleting sms n.$msg_index from storage ".($storage || "default")." (result: `$ans') => " . ($ok ? 'ok' : '*FAILED*') );
	return $ok;
}

sub _get_result_of_at {
	my ( $self,$at ) = @_;
	my ( $code,$result );
	if( $self->_test_command('+'.$at) ) {
		$self->atsend('AT+'.$at.Device::Modem::CR);
		( $code,$result ) = $self->parse_answer($Device::Modem::STD_RESPONSE,5000);
		$self->log->write('info','AT+'.$at.": $result");
	}
	return $result || $code;
}


sub _test_command {
	my($self, $command) = @_;

	if( $command =~ /^[a-zA-Z]/ ) {
		$command = '+'.$command;
	}

	# Standard test procedure for every command
	$self->log->write('info', 'testing support for command ['.$command.']');
	$self->atsend( "AT$command=?" . Device::Modem::CR );

	# If answer is ok, command is supported
	my $ok = ($self->answer($Device::Modem::STD_RESPONSE) || '') =~ /OK/o;
	unless ( $ok ) {
		$self->atsend( "AT$command" . Device::Modem::CR );
		$ok = ($self->answer($Device::Modem::STD_RESPONSE) || '') =~ /OK/o;
	}
	unless ( $ok ) {
		$self->atsend( "AT$command?" . Device::Modem::CR );
		$ok = ($self->answer($Device::Modem::STD_RESPONSE) || '') =~ /OK/o;
	}
	$self->log->write('info', 'command ['.$command.'] is '.($ok ? '' : 'not ').'supported');

	$ok;
}


sub _encode_pdu {
	my($self, %opt) = @_;

	# Get options
	my $num =  $opt{'recipient'};
	my $text = $opt{'content'};

	return 0 unless $num and $text;

	# Select class of sms (normal or *flash sms*)
	my $class = $opt{'class'} || 'normal';
	$class = $class eq 'normal' ? '00' : 'F0';

	# Validity period (Max value)
	my $vp = 'FF';

	# Status report requested?
	my $status_report = 0;
	if( exists $opt{'status_report'} && $opt{'status_report'} )
	{
		$status_report = 1;
	}

	# Send sms in PDU mode

	# Encode DA
	my $enc_da = Device::Gsm::Pdu::encode_address( $num );
	$self->log->write('info', 'encoded dest. address is ['.$enc_da.']');

	# Encode text

	my $enc_msg =unpack( "H*",encode("UCS2",decode( 'utf8',$text )) );
	$self->log->write('info', 'encoded multi-bytes text  is ['.$enc_msg.']');

	# Build PDU data
	my $pdu = uc join(
		'',
		'00',
		($status_report ? '31' : '11'),
		'00',
		$enc_da,
		'00',
		'08',
		$vp,
		sprintf( "%02X",(length $enc_msg)/2 ),
		$enc_msg
	);
	
	return $pdu;

}

sub flush {
	my $self = shift;
	$self->mode('pdu');
	$self->atsend( q{AT+CNMI='2,2,0,0,0''} . Device::Modem::CR);
	my $ok = ($self->answer($Device::Modem::STD_RESPONSE) || '') =~ /OK/o;

	if( $ok ) {
		my @msg;
		while( !@msg ){
		 @msg= $self->answer(qr/CMTI/,3600);
		print Dumper \@msg if @msg;
		}
		die;
	my $messages;
		# Ok, messages read, now convert from PDU and store in object
		my @data = split /[\r\n]+/m, $messages;
		# Check for errors on SMS reading
		my $code;
		if( ($code = pop @data) =~ /ERROR/ ) {
			$self->log->write('error', 'cannot read SMS messages on SIM: ['.$code.']');
			return ();
		}
	
		my @message = ();
		my $current;
	
	# Current sms storage memory (ME or SM)
	my $storage = $self->storage();
	
		#
		# Parse received data (result of +CMGL command)
		#
		while( @data ) {
			$self->log->write('debug', 'data[] = ', $data[0] );
			my $header=shift @data;
			my $pdu   =shift @data;
			# Instance new message object
			my $msg = new Device::Gsm::Sms(
				header => $header,
				pdu    => $pdu,
		# XXX mode   => $self->mode(),
		storage => $storage,
		parent => $self                  # Ref to parent Device::Gsm class
			);
	
			# Check if message has been instanced correctly
			if( ref $msg ) {
				my $SCTS = $msg->{tokens}->{SCTS};
				my $datatime = $SCTS->{_day}."/".$SCTS->{_month}."/".($SCTS->{_year}+2000)." ".$SCTS->{_time};
				push @message, {
					index => $msg->{index},
					sendTime => $datatime,
					senderNum => $msg->{tokens}->{OA}->{_address},
					content => encode_utf8( $msg->{tokens}->{UD}->{_text} ),
				};
			} else {
				$self->log->write('info', "could not instance message $header $pdu!");
			}
		}
		$self->log->write('info', 'found '.(scalar @message).' messages on SIM. Reading.');
		return @message;
	}
}


1;