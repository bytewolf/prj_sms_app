package MyGSM::Model;

use strict;
use base qw /DBIx::Abstract/;

sub init_db {
	my ( $self,$dsn ) = @_;
	return ( 
			ref $dsn eq "HASH" 
			? __PACKAGE__->connect($dsn)
			: 0
	)
}

1;
