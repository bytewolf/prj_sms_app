# Accounts keeping via SMS. #

If you send SMS messages of specific formats to the cellphone number which belongs to a SIM card on a GSM modem connected to a Linux computer via RS232, this prototype can decide what to do next based on the message types:

- if it's an accounting related msg, then insert/update to the database.

- if it's a report related msg, then generate the incoming/expense report and SMS it back to the sender.

A web application is also running on that Linux computer for you to manage your accounts via browser.